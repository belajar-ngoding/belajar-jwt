<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\Route;

$router->get('/', function () use ($router) {
    $date = \Carbon\Carbon::now('Asia/Jakarta')->format('Y-m-d H:i:s');
    $date2 = \Carbon\Carbon::parse($date, 'Asia/Jakarta')->timestamp;
    \Illuminate\Support\Facades\Log::info($date);
    
    return date('Y-m-d H:i:s', $date2);
});
Route::post('/register', 'RegisterController@register');

Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function() use($router) {
    Route::post('login', 'AuthController@login');
    
    Route::group(['middleware' => 'auth:api'], function () use($router) {
        Route::get('logout', 'AuthController@logout');
        Route::post('refresh', 'AuthController@refresh');
        Route::post('me', 'AuthController@me');
    });
});

$router->get('/key', function() {
    return \Illuminate\Support\Str::random(32);
});