<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InitGlobalUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->updateOrInsert([
            'full_name' => 'Global Admin',
            'username' => 'globaladmin',
            'password' => app('hash')->make('Admin@123'),
            ]);
    }
}
