<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class LogMiddleware
{
    protected $except_urls = [
        'v1/detail',
    ];

    public function handle($request, Closure $next)
    {
        $uid = Str::random(12);

        $response = $next($request);
        
        Log::info($uid, [
            'header' => $request->header(),
            'ip' => $request->ip(),
            'url' => $request->fullUrl(),
            'query' =>  $request->query(),
            'body' => $request->except(['password', 'c_password']),
            'response' => $response
        ]);

        return $response;
    }
}
