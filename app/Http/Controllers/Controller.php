<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{

    protected $statusCode = 200;

    protected function respondWithToken($token)
    {
        return response()->json([
            'status' => 'success',
            'status_code' => 200,
            'token' => $token,
            'token_type' => 'bearer',
            'expires_in' => time() + 60*60,
        ], 200);
    }

    protected function unAuthicated()
    {
        return response()->json([
            'status' => 'error',
            'status_code' => 422,
            'message' => 'Incorrect Username or Password Combination being used'
        ], 422);
    }

    protected function logoutResponse()
    {
        return $this->setStatusCode(200)->makeResponse(null, 'User Successfully Signout');
    }

    protected function respondUnauthorized($message = 'Unauthorized!', $headers = [])
    {
        return $this->setStatusCode(401)->makeResponse(null, $message, $headers, 'error');
    }

    protected function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    protected function makeResponse($data = null, $message = null, $headers = [], $result = 'success')
    {
        $result = [
            'status' => $result,
            'status_code' => $this->statusCode,
        ];
        if(!empty($message)) $result['message'] = $message;
        $result['data'] = $data;

        return $this->respond($result, $headers);
    }

    protected function respondValidationError($data = [], $message = 'Validation Error!', $headers = [])
    {
        $result = [
            'status' => 'error',
            'status_code' => 422,
            'message' => $message,
            'data' => [$data]
        ];

        return response()->json($result, 422, $headers);
    }

    protected function respond($data, $headers = [])
    {
        return response()->json($data, $this->statusCode, $headers);
    }
}
