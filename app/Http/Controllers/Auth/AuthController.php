<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    /**
     * create auth
     */


    /**
     *
     * login  function
     *
     * @param username phone,email,password
     * @param password min 8
     */

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    public function login(Request $request)
    {
        try {
            // $credentials = request(['username', 'password']);
            $this->validate($request, [
                'username' => 'required',
                'password' => 'required|min:6'
            ]);

            if (! $token = Auth::attempt(['username' => $request->username, 'password' => $request->password])) {
                return $this->unAuthicated();;
            }else {
                return $this->respondWithToken($token);
            }
        } catch (ValidationException $ex) {
            return $this->respondValidationError($ex->response->original);
        }


    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(Auth::user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        Auth::logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(Auth::refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    // protected function respondWithToken($token)
    // {
    //     return response()->json([
    //         'access_token' => $token,
    //         'token_type' => 'bearer',
    //         'expires_in' => Auth::factory()->getTTL() * 60
    //     ]);
    // }
}
