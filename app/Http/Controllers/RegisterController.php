<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function register(Request $request)
    {
        $new_user = new User();
        $new_user->full_name = $request->full_name;
        $new_user->username = $request->username;
        $new_user->password = Hash::make($request->password);
        $new_user->save();
        // return ($new_user);

        return response()->json($new_user);
    }
}
